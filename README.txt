================================================================================
+++++ WHOLE-BRAIN ANALYSIS ROUTINE FOR QUANTITATIVE SUSCEPTIBILITY MAPPING +++++
================================================================================

This skeleton provides guidance through the necessary steps for cluster-based 
 (whole-brain) and regional analysis of QSM data

If you found this code useful for your research, please cite:
 Acosta-Cabronero et al. J Neurosci 2016
  where this pipeline was first described 


USAGE
=====

In a terminal window, change to the present working directory, and run:

chmod +x setmode.sh; ./setmode.sh

setmode.sh makes all BASH scripts in this skeleton executable

Next, take a look at the README files in each directory and follow the 
 instructions. Open and read carefully each script before running. Edit when 
  necessary - custom settings are usually listed at the top of each script


DEPENDENCIES
============

bash_utils    in pwd
matlab_utils  in pwd
SPM           Statistical Parametric Mapping	http://www.fil.ion.ucl.ac.uk/spm
FSL           FMRIB Software Library		http://fsl.fmrib.ox.ac.uk
ANTs          Advanced Normalization Tools 	http://stnava.github.io/ANTs

[OPTIONAL]
SGE	      Sun Grid Engine


NOTES
=====

This code was developed in Ubuntu Linux 14.04 with local installations of SPM12 
 v6080, FSL v5.0.9 and ANTs v2.1.0. The latter two software packages can send
  jobs to an SGE scheduler for parallel computing or single-node multi-core 
   management. For details on the latter, see:
    https://scidom.wordpress.com/2012/01/18/sge-on-single-pc/


THANKS
======

Important contributions from:

Renat Yakupov, German Center for Neurodegenerative Diseases (DZNE), Magdeburg
George Thomas, Dementia Research Centre, University College London


CONTACT
=======

jac at cantab.net

================================================================================
++++++++++++++++ Created by Julio Acosta-Cabronero (21/10/2016) ++++++++++++++++
+++++++++++++ Last modified by Julio Acosta-Cabronero (14/02/2019) +++++++++++++
================================================================================

