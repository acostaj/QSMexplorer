Permutation statistics
======================

1. Move smoothing-compensated 4D NIFTI files (all_cs*.nii.gz) and brainmask.nii.gz in 03_warp to the present directory


2. Prepare GLM design

   - For two-group comparison: $ design_ttest2 Ngroup1 Ngroup2

   - Otherwise, use Glm (http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/GLM) 


3. Run randomise (http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Randomise)


[OPTIONAL] Warp whole-brain stats map to the MNI co-ordinate system

4. Generate study-wise template to MNI warp transforms going through: 05_postwarp/[01_brainextract + 02_MNI]

5. Return to 04_WB_stats/warp_MNI, edit and run 01_warp+crop.sh


Examples:


+++ Two-group comparison (in 01_randomise_two-group_comparison_example.sh ) +++++++++++++++++++++++++++++++++++++++++++++++++++++

Ngroup1=20
Ngroup2=20
Nperm=5000
msk=brainmask

echo +++ Set-up design
design_ttest2 design $Ngroup1 $Ngroup2

echo +++ Permutation statistics
for data in all_cs3_abs_brain_wqsm25 all_cs3_brain_wqsm25; do echo Stats: $i
        if [ ! -f ${data}.nii.gz ]; then
		mv ../03_warp/${data}.nii.gz ./
	fi
        cp ../03_warp/${msk}.nii.gz  ./
        randomise_parallel -i ${data}.nii.gz -o stats_${data}_design -m ${msk}.nii.gz -d design.mat -t design.con -n ${Nperm} -T --uncorrp
done

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


+++ Age regression  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

<<C
NOTE1: Use Glm to produce design files (see http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/GLM)
NOTE2: Demean ages before entering into Glm
NOTE3: The -D flag in randomise ensures data is also demeaned before model fitting
C

Nperm=5000
msk=brainmask
designroot=age

echo +++ Permutation statistics
for data in all_cs3_abs_brain_wqsm25 all_cs3_brain_wqsm25; do echo Stats: $i
        if [ ! -f ${data}.nii.gz ]; then 
		mv ../03_warp/${data}.nii.gz ./
	fi
        cp ../03_warp/${msk}.nii.gz  ./
	randomise_parallel -i ${data}.nii.gz -o stats_${data}_age -m ${msk}.nii.gz -d ${designroot}.mat -t ${designroot}.con -n ${Nperm} -T -D --uncorrp
done

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

