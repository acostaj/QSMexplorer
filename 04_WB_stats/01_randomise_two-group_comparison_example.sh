<<C
Custom settings
C

Ngroup1=20
Ngroup2=20
Nperm=5000
msk=brainmask

echo "+++ Import spatially normalised QSM template (v-SMV R0=25 mm) to overlay results on"
ln -sf ../03_warp/median_all_brain_wqsm25.nii.gz

echo +++ Set-up design
design_ttest2 design $Ngroup1 $Ngroup2

echo +++ Permutation statistics
for data in all_cs3_abs_brain_wqsm25 all_cs3_brain_wqsm25; do echo Stats: $data
        if [ ! -f ${data}.nii.gz ]; then
                mv ../03_warp/${data}.nii.gz ./
        fi
        cp ../03_warp/${msk}.nii.gz  ./
        randomise_parallel -i ${data}.nii.gz -o stats_${data}_design -m ${msk}.nii.gz -d design.mat -t design.con -n $Nperm -T --uncorrp
done
