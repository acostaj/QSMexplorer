<<C
Custom settings
C

W=MNI_to_T1template


echo "+++ Import data"
ln -sf ../../05_postwarp/02_MNI/${W}_* ./
ln -sf ../../03_warp/median_all_brain_wqsm*.nii.gz ./
ln -sf ../../05_postwarp/01_brainextract/brain_T1template.nii.gz

echo "+++ Apply transform (study space => MNI)"

echo + QSM templates
for I in median_all_brain_wqsm*.nii.gz; do echo $I

antsApplyTransforms -d 3 -i $I -o wMNI_${I} -r MNI152_T1_1mm_brain.nii.gz \
        				    -t [${W}_1InverseWarp.nii.gz,0] [${W}_0GenericAffine.mat,1] \
        				    -n BSpline
done


echo + T1 template
I=brain_T1template.nii.gz
echo $I

antsApplyTransforms -d 3 -i $I -o wMNI_${I} -r MNI152_T1_1mm_brain.nii.gz \
        				    -t [${W}_1InverseWarp.nii.gz,0] [${W}_0GenericAffine.mat,1] \
        				    -n BSpline

<<C
Add other templates if you wish (remember to import first)
C

echo "+++ Crop images"
for i in wMNI_*.nii.gz; do echo $i
	fslroi $i crop_${i} 18 142 21 178 0 160
done
