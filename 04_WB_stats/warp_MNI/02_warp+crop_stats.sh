<<C
Custom setting
C

W=MNI_to_T1template


echo "+++ Apply transform (study space => MNI)"
echo "Stats warp: contrast 2 (group2>group1), FWE corrected, edge-compensated 3-mm sigma Gaussian-smooothed absolute QSM with 25-mm starting radius v-SMV filtering"
I=stats_all_cs3_abs_brain_wqsm25_design_tfce_corrp_tstat2.nii.gz
ln -sf ../$I
antsApplyTransforms -d 3 -i $I -o wMNI_${I} -r MNI152_T1_1mm_brain.nii.gz \
        				    -t [${W}_1InverseWarp.nii.gz,0] [${W}_0GenericAffine.mat,1] \
        				    -n Linear

echo "Stats warp: contrast 2 (group2>group1), FWE corrected, edge-compensated 3-mm sigma Gaussian-smooothed QSM with 25-mm starting radius v-SMV filtering"
I=stats_all_cs3_brain_wqsm25_design_tfce_corrp_tstat2.nii.gz
ln -sf ../$I
antsApplyTransforms -d 3 -i $I -o wMNI_${I} -r MNI152_T1_1mm_brain.nii.gz \
        				    -t [${W}_1InverseWarp.nii.gz,0] [${W}_0GenericAffine.mat,1] \
        				    -n Linear


echo "+++ Crop images"
for i in wMNI_stats*.nii.gz; do echo $i
        fslroi $i crop_${i} 18 142 21 178 0 160
done


echo
echo "NOTE: If you wish to extend for additional statistical maps, the easiest way would be to copy this script to 02b_warp+crop_stats.sh, edit accordingly and run"
echo

