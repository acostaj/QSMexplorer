PATH=$PATH:$PWD/../bash_utils

for i in `ptb_Dirs`; do echo $i
	cd $i
	for j in *.nii.gz; do echo $j
		fslreorient2std $j $j
	done
	cd ..
done
