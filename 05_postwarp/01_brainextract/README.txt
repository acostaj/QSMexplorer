T1 template brain extraction
============================

Brain extraction will be performed for the T1 template with the following methods:

1. ANTs' antsBrainExtraction.sh using LPBA40 priors

2. (1) + FSL's brain extraction tool (BET2, fractional threshold: 0.1-0.5)


All outputs will be loaded into FSLView


IMPORTANT: Rename optimal brain extracted template as brain_T1template.nii.gz
