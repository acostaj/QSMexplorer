<<C
Custom setting
C

Troot=T1template

echo +++ Import data
ln -sf ../../02_ANTs/${Troot}.nii.gz ./

echo "+++ ANTs' antsBrainExtraction.sh using LPBA40 priors"
time antsBrainExtraction.sh -d 3 -a ${Troot}.nii.gz -e lpba40_ants_template.nii.gz -m lpba40_ants_template_brainprobmask.nii.gz -o ${Troot}

echo "+++ FSL's brain extraction tool (BET2, fractional threshold: 0.1-0.5)"
for i in 1 2 3 4 5; do echo "BET2, f0.$i"; time bet2 ${Troot}BrainExtractionBrain.nii.gz bet2f0.${i}_${Troot}BrainExtractionBrain.nii.gz -f 0.${i}; done

echo +++ Visualisation
fslview ${Troot}.nii.gz ${Troot}BrainExtractionBrain.nii.gz bet2*.nii.gz &

echo
echo "IMPORTANT: Rename optimal brain extracted structural template as brain_${Troot}.nii.gz (it will be used in subsequent steps)"
echo
echo "NOTE: In past 3T studies, bet2f0.4_${Troot}BrainExtractionBrain.nii.gz was selected"
echo
