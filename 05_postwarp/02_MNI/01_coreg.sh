echo "++++ Study-wise T1 template => MNI co-registration"

<<C
Custom setting
C

Nthreads=4


echo +++ Import data
ln -sf ../../02_ANTs/T1template.nii.gz ./
ln -sf ../01_brainextract/brain_T1template.nii.gz ./

echo +++ Co-registration operation
time antsRegistrationSyN.sh -d 3 -f T1template.nii.gz -m MNI152_T1_0.5mm.nii.gz -o MNI_to_T1template_ -t s -n $Nthreads

echo +++ Visualisation
fslview T1template.nii.gz MNI_to_T1template_Warped.nii.gz &

echo
echo +++ NOTE: If co-registration was unsatisfactory, could try with brain-only templates instead, i.e.:
echo
echo time antsRegistrationSyN.sh -d 3 -f brain_T1template.nii.gz -m MNI152_T1_1mm_brain.nii.gz -o brain_MNI_to_T1template_ -t s -n $Nthreads
echo
