<<C
Custom setings
C

Troot=brain_T1template
Nthreads=4


echo +++ Import data
ln -sf ../01_brainextract/${Troot}.nii.gz ./

echo +++ Co-registration with brain-only templates
time antsRegistrationSyN.sh -d 3 -f brain_OASIS-30.nii.gz -m ${Troot}.nii.gz -o ${Troot}_to_OASIS-30_ -t s -n $Nthreads

echo +++ Visualisation
fslview brain_OASIS-30.nii.gz ${Troot}_to_OASIS-30_Warped.nii.gz &

echo
echo If co-registration result is satisfactory, run 02_applywarp+finalcleanup.sh
echo
echo Otherwise, try with whole-head data as follows:
echo
echo Troot=T1template
echo ln -sf ../../02_ANTs/${Troot}.nii.gz ./
echo time antsRegistrationSyN.sh -d 3 -f OASIS-30.nii.gz -m ${Troot}.nii.gz -o ${Troot}_to_OASIS-30_ -t s -n $Nthreads
echo fslview brain_OASIS-30.nii.gz ${Troot}_to_OASIS-30_Warped.nii.gz brain_${Troot}_to_OASIS-30_Warped.nii.gz
echo
echo If the latter was preferred, edit 02_applywarp+finalcleanup.sh custom settings appropriately and run
echo
