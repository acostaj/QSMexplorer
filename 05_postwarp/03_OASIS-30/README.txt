Transform atlas labels for regional extraction
==============================================

Edit 01_coreg.sh, and run

01_coreg.sh will compute the warp transformation from OASIS-30 to study-wise space

02_applywarp+finalcleanup.sh will then transform Desikan-Killiany-Tourville (DKT) labels (Klein and Tourville Front Neurosci 2012, see DKT_labels.txt in pwd) to the latter and will intersect them with GM-only and QSM masks, respectively, to enable appropriate regional extraction from the cortex

In addition, the DKT protocol also includes subcortical/cerebellar regions though note, in 06_ROI_extract/01_FIRST, subcortical ROIs will also be computed which are preferred

All labels will be transformed to MNI space

The present skeleton also serves as a template for regional extraction using any other parcellation protocol (e.g. Brodmann, AAL)

If using a WM atlas, intersection with the GM mask should be omitted. In this context, applying a WM mask (from DTI or from SPM's c2) would be desirable
