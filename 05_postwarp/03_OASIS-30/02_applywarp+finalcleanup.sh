echo "++++ Transform Desikan-Killiany-Tourville labels to study-wise and MNI co-ordinate systems"

<<C
Custom settings
C

Troot=brain_T1template
W=${Troot}_to_OASIS-30
R=OASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz


echo "+++ Apply transforms (atlas labels => study-wise space)"
antsApplyTransforms -d 3 -i $R -r ${Troot}.nii.gz -o w${R} -n NearestNeighbor -t [${W}_0GenericAffine.mat,1] [${W}_1InverseWarp.nii.gz,0]

echo "+++ Import data"
ln -sf ../../03_warp/mean_all_wc1t1.nii.gz ./
ln -sf ../../03_warp/brainmask.nii.gz ./
ln -sf ../../03_warp/median_all_brain_wqsm5.nii.gz ./

echo "+++ Intersect warped atlas labels with group-average GM tissue and QSM brain masks"
fslmaths mean_all_wc1t1.nii.gz -thr 0.5 -bin -mul brainmask.nii.gz -mul w${R} final_w${R}

echo "+++ Transform atlas labels to MNI space"
flirt -in ${R} -ref MNI152_T1_1mm_brain.nii.gz -applyxfm -init brain_OASIS-30_to_MNI152.mat -interp nearestneighbour -out wMNI_${R}

echo "+++ Visualisation (clip colorscale for QSM template between -0.1 to 0.1"
fslview median_all_brain_wqsm5.nii.gz final_w${R} -l "Random-Rainbow" &


echo
echo "DKT labels ready for QSM extraction: final_w${R}"
echo
echo "DKT labels in MNI space (no clean-up): wMNI_${R}"
echo
