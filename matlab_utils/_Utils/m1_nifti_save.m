function m1_nifti_save( new_mat, ima_struct, fileroot )
% Save MATLAB array as NIFTI(.gz) file using SPM
%
% SINTAX
% m1_nifti_save( new_mat, ima_struct, fileroot )
%
% OUTPUT (file)
% filename = [ fileroot '.nii.gz' ]
% 
% REQUIREMENTS
%  SPM
%  Gzip (UNIX command line)
%
% > Created by Julio Acosta-Cabronero (30/12/2013)


disp([ '> Save ' fileroot ])

    % If image structure was derived from 4D image, use structure of first
    %  image only to save 3D output data 
    if length( size( new_mat ))==3
        ima_struct = ima_struct(1);
    end
    
    fname = [ fileroot '.nii' ];   
    ima_struct.fname = fname;
    ima_struct.private.dat.fname = fname;
    
    ima_struct.dim = size( new_mat );
    ima_struct.private.dat.dim = size( new_mat );
    
    ima_struct.descrip = 'PTB';
    ima_struct.private.descrip = 'PTB';
    
    spm_write_vol( ima_struct, new_mat );
    
    eval([ '!gzip -f ' fname ])
%     delete([ fname ])
    
end
