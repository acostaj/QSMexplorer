Regional extraction
===================

1. Perform FSL-FIRST segmentation (Patenaude et al. Neuroimage 2011) on structural template (01_FIRST in pwd). Note 4D merged all_brain_wqsm*.nii.gz files are in the study-wise space defined by T1template.nii.gz

2. Manually trace structures that are not included in the DKT protocol or in FIRST (02_manual in pwd) e.g. substantia nigra, red nucleus, cerebellar dentate nucleus, subthalamic nucleus, or subsegmentations of the globus pallidus or thalamus. 

3. Edit carefully 03_import.sh to import 4D NIFTI files you want to extract data from, e.g. all_brain_wqsm25.nii.gz, and multi-ROI maps e.g. FIRST segmentation output from 01_FIRST, manually traced masks from 02_manual (not included at present in 03_import.sh) and/or cortical ROIs from the DKT atlas in 05_postwarp/03_OASIS-30

4. Edit and run ROI_extract.m for each data/multi-ROI pair

