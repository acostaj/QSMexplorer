Manual tracing
==============

1. Import template you want to trace ROIs on

2. Trace ROIs and save as NIFTI

3. If multiple NIFTI files were created, and if they are not overlapping, it will be more convenient for extraction to merge them into a single multi-ROI file
