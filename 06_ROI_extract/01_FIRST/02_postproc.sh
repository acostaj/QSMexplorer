PATH=$PATH:$PWD/../../bash_utils

<<C
Custom setting
C

Troot=brain_T1template


echo +++ Erode ROIs in 3D by 1-mm
ptb_Erode ${Troot}_all_fast_firstseg.nii.gz 1

echo +++ Intersection with QSM brain mask
fslmaths ero1_${Troot}_all_fast_firstseg.nii.gz -mul brainmask.nii.gz ero1_${Troot}_all_fast_firstseg.nii.gz

echo "+++ Visualisation (clip QSM template range at [-0.1 0.1]"
fslview ${Troot}.nii.gz median_all_brain_wqsm25.nii.gz ero1_${Troot}_all_fast_firstseg.nii.gz -l "Random-Rainbow" &


echo
echo "======= FIRST LABELS ======="
echo + 10 Left Thalamus 
echo + 11 Left Caudate Nucleus  
echo + 12 Left Putamen  
echo + 13 Left Globus Pallidus  
echo + 16 Midbrain      
echo + 17 Left Hippocampus
echo + 18 Left Amygdala 
echo + 26 Left Nucleus Accumbens        
echo + 49 Right Thalamus        
echo + 50 Right Caudate Nucleus 
echo + 51 Right Putamen 
echo + 52 Right Globus Pallidus 
echo + 53 Right Hippocampus     
echo + 54 Right Amygdala        
echo + 58 Right Nucleus Accumbens
echo
