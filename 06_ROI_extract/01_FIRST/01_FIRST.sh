PATH=$PATH:$PWD/../../bash_utils

echo +++ Import data
ln -sf ../../05_postwarp/01_brainextract/brain_T1template.nii.gz
ln -sf ../../03_warp/median_all_brain_wqsm25.nii.gz
ln -sf ../../03_warp/brainmask.nii.gz

echo +++ Run FSL-FIRST
time run_first_all -b -3 -i brain_T1template.nii.gz -o brain_T1template

echo
echo "NOTE: If SGE listening, this job may have been submitted via qsub"
echo "       		       use qstat to monitor progress"
echo
echo "When FIRST finished, check output:"
echo
echo "fslview brain_T1template.nii.gz.nii.gz brain_T1template_all_fast_firstseg.nii.gz -l Random-Rainbow"
echo

echo
echo "======= FIRST LABELS ======="
echo + 10 Left Thalamus 
echo + 11 Left Caudate Nucleus  
echo + 12 Left Putamen  
echo + 13 Left Globus Pallidus  
echo + 16 Midbrain      
echo + 17 Left Hippocampus
echo + 18 Left Amygdala 
echo + 26 Left Nucleus Accumbens        
echo + 49 Right Thalamus        
echo + 50 Right Caudate Nucleus 
echo + 51 Right Putamen 
echo + 52 Right Globus Pallidus 
echo + 53 Right Hippocampus     
echo + 54 Right Amygdala        
echo + 58 Right Nucleus Accumbens
echo

echo
echo "If poor performance segmenting brain-only template, could try with whole-head data as follows:"
echo
echo "ln -sf ../../02_ANTs/T1template.nii.gz ./"
echo "time run_first_all -3 -i T1template.nii.gz -o T1template"
echo
echo "If the above was preferred, i.e. FIRST segmentation of whole-head T1template, edit custom setting in 02_postproc.sh, and run"
echo
echo "Otherwise, i.e. if FIRST segmentation of brain_T1template was satisfactory, run 02_postproc.sh without editing"
echo

