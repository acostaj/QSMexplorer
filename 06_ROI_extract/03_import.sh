<<C
WARNING: Edit carefully
C

echo +++ Import QSM data
for i in all_abs_brain_wqsm25.nii.gz all_brain_wqsm25.nii.gz; do echo $i
	if [ ! -f $i ]; then
		mv ../03_warp/$i ./
	fi
done

echo +++ Import ROIs
cp -v ../05_postwarp/03_OASIS-30/final_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2.nii.gz ./
cp -v 01_FIRST/ero1_brain_T1template_all_fast_firstseg.nii.gz ./
