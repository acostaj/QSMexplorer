% ROI EXTRACTION PROGRAM
%  This program will extract data from a 4D NIFTI file (data_root)
%  Regions of interest must be provided (ROI_root)
%
% > Created by Julio Acosta-Cabronero (24/10/2016)

clear all
tic

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EDIT THIS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input data
data_root = 'all_brain_wqsm25';
% data_root = 'all_abs_brain_wqsm25';

% Input ROIs
ROI_root  = 'ero1_brain_T1template_all_fast_firstseg';
% ROI_root  = 'final_wOASIS-TRT-20_jointfusion_DKT31_CMA_labels_in_OASIS-30_v2';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Output
csv_root  = [ROI_root '_FROM_' data_root];

%%
disp('>>> Load data')
addpath('../matlab_utils/_Utils')
addpath('../matlab_utils/_spm12b_nifti')
[data] = m1_nifti_load(data_root);
[ROI]  = m1_nifti_load(ROI_root);

%%
disp('>>> ROI extraction')
minROI = min(ROI(ROI~=0));
maxROI = max(ROI(:));

c = 0;
% Loop across min-max ROI value range
for x = minROI:maxROI
    idx = find(ROI==x);
    % Check if ROI exits
    if (idx)
        disp(['ROI: ' num2str(x)])
        c = c+1;
        % Loop across subjects
        for y = 1:size(data,4)
            data0 = data(:,:,:,y);
            if y == 1
                mat(1,c,1:6) = x;
            end
            mat(y+1,c,1) = mean(data0(idx));
            mat(y+1,c,2) = median(data0(idx));
            mat(y+1,c,3) = std(data0(idx));
            mat(y+1,c,4) = iqr(data0(idx));
            mat(y+1,c,5) = prctile(data0(idx),25);
            mat(y+1,c,6) = prctile(data0(idx),75);
        end
    end
end

%%
disp('>>> Save spreadsheets')
csvwrite([csv_root '_MEAN.csv'],mat(:,:,1));
csvwrite([csv_root '_MEDIAN.csv'],mat(:,:,2));
csvwrite([csv_root '_STD.csv'],mat(:,:,3));
csvwrite([csv_root '_IQR.csv'],mat(:,:,4));
csvwrite([csv_root '_25PRCTILE.csv'],mat(:,:,5));
csvwrite([csv_root '_75PRCTILE.csv'],mat(:,:,6));

%%
toc
