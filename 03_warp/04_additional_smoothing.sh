d0=$PWD

<<C
Additional kernel sizes
C

kSize="1.5 5"


echo "++++ Smoothing operations"
for i in `cat sorted_list.txt`; do echo +++ $i
	cd $i
	
	echo "++ $i: Gaussian smoothing"
	for j in wfinal2_msk.nii.gz brain_wqsm*.nii.gz abs_brain_wqsm*.nii.gz; do echo $j
		for k in $kSize; do echo sigma: $k
			fslmaths $j -s ${k} s${k}_${j}
	done; 	done

	echo "++ $i: Smoothing compensation"
	for j in brain_wqsm*.nii.gz abs_brain_wqsm*.nii.gz; do echo $j
                for k in $kSize; do echo sigma: $k
			fslmaths s${k}_${j} -div s${k}_wfinal2_msk.nii.gz cs${k}_${j}
	done; 	done

	cd $d0
done

echo "++++ Merge subject data"
for k in $kSize; do echo sigma: $k
	cd subj_001*
	for i in cs${k}_*.nii.gz; do echo $i
        	cd $d0
		fslmerge -t all_${i} subj_*/$i
done; 	done
