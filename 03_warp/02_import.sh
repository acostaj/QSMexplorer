PATH=$PATH:$PWD/../bash_utils
d0=$PWD

cd ../00_data
for i in `ptb_Dirs`; do echo $i
	ptb_MDir $d0/$i
	cd $d0/$i
	ln -sf ../../00_data/$i/magn.nii.gz
	ln -sf ../../00_data/$i/qsm*.nii.gz ./
	ln -sf ../../00_data/$i/final_msk.nii.gz
	ln -sf ../../01_SPM/${i}/m${i}*.nii.gz  mt1.nii.gz
	ln -sf ../../01_SPM/${i}/c1${i}*.nii.gz c1t1.nii.gz
	ln -sf ../../01_SPM/${i}/c2${i}*.nii.gz c2t1.nii.gz
        ln -sf ../../02_ANTs/T1template.nii.gz T1template.nii.gz
        ln -sf ../../02_ANTs/T1m${i}Affine.* t12template_Affine.txt
	ln -sf ../../02_ANTs/T1m${i}Warp.nii.gz t12template_Warp.nii.gz
done
