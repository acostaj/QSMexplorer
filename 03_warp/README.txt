Spatial normalisation and smoothing
===================================

This steps prepare QSM data for analysis:

1. Populate a column text file, sorted_list.txt, with all subjects IDs sorted in the order you would like them to appear in the merged 4D NIFTI files. If for instance, you are planning to perform a two-group comparison, list all subject IDs from group 1 first, then list those for group 2. Remember when preparing your design in 04_stats, the order covariates are introduced into the design should be exactly the same.

2. Import data, run ./02_import.sh

3. Process data, run ./03_proc.sh

NOTE: In 03_proc.sh a few parameters could be twicked, though in principle the prescribed arguments should be reasonably robust. One thing to note is that the default setting is to smooth data by convolution with a 3-mm standard deviation 3D Gaussian kernel. Please see Betts et al. Neuroimage 2016 for a comparison between kernel widths with 7T QSM/R2* data in an aging cohort.

4. If you wish to try additional smoothing kernel sizes, edit and run ./04_additional_smoothing.sh


[OPTIONAL] 

The dipole kernel that defines the forward magnetostatic model is undetermined for the DC component (kx=0,ky=0,kz=0). It is thus generally recommended to normalise QSM values (by direct subtraction) to the average QSM value in a reference region. Such reference normalisation step is not standardised and poses problems when study patient populations, i.e. when there is no certainty a particular region is untouched by disease. The recommended setting in this pipeline is therefore NOT to normalise. A previous study suggested such reference adjustment might be small relative e.g. to aging related QSM alterations (Acosta-Cabronero J Neurosci 2016). Yet if the user wishes to normalise, the easiest approach would be doing so after importing the data, and naming e.g. referenced qsm25.nii.gz as qsm25r.nii.gz for each subject. Note 03_proc.sh will be looping through qsm*.nii.gz files, thus it will generate merged NIFTI files both for raw and referenced data.
