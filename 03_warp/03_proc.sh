PATH=$PATH:$PWD/../bash_utils
d0=$PWD

<<C
Nthreads: Number of threads to use for antsRegistrationSyN.sh
C

Nthreads=4

echo "++++ Spatial normalisation and smoothing operations"
for i in `cat sorted_list.txt`; do echo +++ $i
	cd $i

	echo "++ $i: RF bias field correction (GRE magnitude)"
	time N4BiasFieldCorrection -i magn.nii.gz -o mmagn.nii.gz -r 1 -s 4 -c [150x150x150x150,0.0] -b [1x1x1,3] -t [0.15,0.01,200]	

	echo "++ $i: Rigid+nonlinear coregistration (GRE magnitude => T1)"
	time antsRegistrationSyN_local.sh -d 3 -f mt1.nii.gz -m mmagn.nii.gz -o magn2t1_ -t sr -j 0 -r 4 -s 26 -n $Nthreads

	echo "++ $i: Apply warp transform (QSM => template)"
	for j in qsm*.nii.gz; do echo + $j 
		antsApplyTransforms -d 3 -i $j -r T1template.nii.gz -o w${j} -n BSpline -t t12template_Warp.nii.gz t12template_Affine.txt magn2t1_1Warp.nii.gz magn2t1_0GenericAffine.mat
	done

	echo "++ $i: Apply warp transform (QSM mask => template)"
	for j in final_msk.nii.gz; do echo + $j
		antsApplyTransforms -d 3 -i $j -r T1template.nii.gz -o w${j} -n NearestNeighbor -t t12template_Warp.nii.gz t12template_Affine.txt magn2t1_1Warp.nii.gz magn2t1_0GenericAffine.mat
	done
	
	echo "++ $i: Apply warp transform (Brain probability map => template)"
	fslmaths c1t1 -add c2t1 c12t1
	for j in c12t1.nii.gz c1t1.nii.gz; do echo $j
		antsApplyTransforms -d 3 -i $j -r T1template.nii.gz -o w${j} -n BSpline -t t12template_Warp.nii.gz t12template_Affine.txt
	done

	echo "++ $i: Compute brain mask (CSF clean-up)"
        fslmaths wc12t1.nii.gz -thr 0.5 -bin -mul wfinal_msk.nii.gz wfinal2_msk.nii.gz

	echo "++ $i: Apply brain mask to QSM and take absolute"
	for j in wqsm*.nii.gz; do echo $j
		fslmaths wfinal2_msk.nii.gz -mul $j brain_${j}
		fslmaths brain_${j} -abs abs_brain_${j}
	done

	echo "++ $i: Gaussian smoothing"
	for j in wfinal2_msk.nii.gz brain_wqsm*.nii.gz abs_brain_wqsm*.nii.gz; do echo $j
		for k in 3; do echo sigma: $k
			fslmaths $j -s ${k} s${k}_${j}
	done; 	done

	echo "++ $i: Smoothing compensation"
	for j in brain_wqsm*.nii.gz abs_brain_wqsm*.nii.gz; do echo $j
                for k in 3; do echo sigma: $k
			fslmaths s${k}_${j} -div s${k}_wfinal2_msk.nii.gz cs${k}_${j}
			rm -f s${k}_${j}
	done; 	done

	cd $d0
done

echo "++++ Sort subject data"
c=0
for i in `cat sorted_list.txt`; do echo $i
	let c++
	C=`ptb_Zeropad $c $c 3`
	ln -sf $i subj_${C}_${i}
done

echo "++++ Merge subject data"
cd subj_001*
for i in cs*.nii.gz brain_wqsm*.nii.gz abs_brain_wqsm*.nii.gz wfinal2_msk.nii.gz wc1t1.nii.gz; do echo $i
        cd $d0
	fslmerge -t all_${i} subj_*/$i
done

echo "++++ Compute summary stats"
cd subj_001*
for i in brain_wqsm*.nii.gz abs_brain_wqsm*.nii.gz; do echo + $i
	cd $d0
        for j in median mean std; do echo $j
                fslmaths all_${i} -T${j} ${j}_all_${i}
        done
		echo variance
		fslmaths std_all_${i} -mul std_all_${i}  var_all_${i}
                echo coefficient of variation
		fslmaths std_all_${i} -div mean_all_${i} CV_all_${i}
		echo relative variance
		fslmaths CV_all_${i}  -mul std_all_${i}  relvar_all_${i}
done
fslmaths all_wc1t1.nii.gz -Tmean mean_all_wc1t1.nii.gz

echo "++++ Compute brain mask"
fslmaths all_wfinal2_msk.nii.gz -Tmin brainmask.nii.gz
rm -f all_wfinal2_msk.nii.gz

