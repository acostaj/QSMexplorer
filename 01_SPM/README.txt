RF bias field correction and segmentation of structural data
============================================================

1. Import structural data, run ./01_import.sh

2. G-unzip data for SPM, run ./02_gunzip.sh

3. Run SPM 'Segment' for all structural images

   Latest SPM version is recommended (SPM12 at the time this was written)

   Default settings are reasonably robust for most 3T datasets

   For 7T data, bias regularisation could be set to 1e-5 and bias FWHM to 30-mm cutoff

   Make sure a bias-corrected image is also saved

   Consider using matlabbatch to run through all subjects

4. G-zip data, run 04_gzip.sh
