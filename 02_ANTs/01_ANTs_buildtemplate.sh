<<C
Custom setting

NOTE: OPTION=2 are recommended settings for ANTs v2.1.0. This is yet untested - output names may differ, thus 03_warp/02_import.sh may need adaptation
C

OPTION=1


echo +++ Import data
PATH=$PATH:$PWD/../bash_utils
d0=$PWD
cd ../01_SPM
for i in `ptb_Dirs`; do echo $i
	rsync -avr $i/m${i}.nii.gz $d0/
done
cd $d0


echo +++ Co-registration operations
if [ "$OPTION" == 1 ]; then

	M=30x90x30
	S=CC
	G=0.1

	echo "++ Build rigid + affine initT1template.nii.gz (4 runs)"
	mkdir init
	cd init
	cp ../*nii.gz .
	time buildtemplateparallel.sh -d 3 -o initT1 -c 1 -i 4 -r 1 -s $S -t RA -n 0 -g $G -m 1x0x0 *.nii.gz
	cd ..
	mkdir initT1
	cp init/initT1template.nii.gz initT1/
	fslmerge -t initT1/all_T1template.nii.gz init/RA_iteration_*/initT1template.nii.gz
	rm -rf init

	echo "++ Build non-linear T1template.nii.gz (6 runs)"
	time buildtemplateparallel.sh -d 3 -o T1 -c 1 -i 6 -s $S -t GR -n 0 -g $G -m $M -z initT1/initT1template.nii.gz *.nii.gz
	fslmerge -t all_T1template.nii.gz initT1/all_T1template.nii.gz GR_iteration_*/T1template.nii.gz
	mv initT1/initT1template.nii.gz ./
	rm -rf initT1 GR_iteration_* 
	
	echo ++ done.

elif [ "$OPTION" == 2 ]; then

	echo "++ Build non-linear T1template.nii.gz (4 runs)"
	time antsMultivariateTemplateConstruction2.sh -d 3 -o T1 -a 1 -c 1 -e 1 -g 0.25 -i 4 -k 1 -w 1 -q 100x100x70x20 -f 6x4x2x1 -s 3x2x1x0 -n 0 -r 1 -l 1 -m CC -t SyN -y 1 *.nii.gz
	mv T1template0.nii.gz T1template.nii.gz

	echo ++ done.
fi

