Study-wise space creation
=========================

A custom study-wise space can be inferred from all structural data using Advanced Normalization Tools (ANTs)

Template construction using custom setttings of ANTs' buildtemplateparallel.sh routine

This pipeline has previously been used in a number of studies, e.g. Acosta-Cabronero et al. J Neurosci 2016, Betts et al. Neuroimage 2016, Acosta-Cabronero et al. Brain 2017

NOTE: ANTs calls in 01_ANTs_builtemplate.sh are set-up for SGE management (-c 1). If SGE is not installed but the local infrastructure has multiple CPUs available, please check buildtemplateparallel.sh and antsMultivariateTemplateConstruction2.sh help dialogues for optimal usage (e.g. -c 2 -j 4, for 4 parallel threads in localhost)

For SGE installation on a multi-core Debian-based workstation, follow this guide: https://scidom.wordpress.com/2012/01/18/sge-on-single-pc/

